set -e
pwd
# récupération des sources calcom-docker = Dockerfile pour build l'image
if [ ! -f calcom-docker.zip ]; then
  wget https://github.com/calcom/docker/archive/refs/heads/main.zip -O calcom-docker.zip;
fi

if [ ! -f dicalso.zip ]; then
  wget https://gitlab.mim-libre.fr/dimail/dicalso/-/archive/main/dicalso-main.zip -O dicalso.zip;
fi

rm calcom-docker -rf
unzip -qq calcom-docker.zip
mv docker-main calcom-docker
cd calcom-docker

# récupération des sources dicalso
unzip -qq ../dicalso.zip
rmdir calcom
mv dicalso-main calcom

# adaptation du dockerfile foireux
sed -i -E '/^.*db-deploy|seed-app-store.*$/d' Dockerfile
