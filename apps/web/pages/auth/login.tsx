import PageWrapper from "@components/PageWrapper";

import type { PageProps } from "~/auth/login-view";
import Login from "~/auth/login-view";

const Page = (props: PageProps) => <Login {...props} />;
Page.PageWrapper = PageWrapper;

export default Page;
export { getServerSideProps } from "@server/lib/auth/login/getServerSideProps";

// "use client";

// import "@gouvfr-lasuite/integration/dist/css/gaufre.css";
// import "@gouvfr-lasuite/integration/dist/css/homepage-gaufre.css";
// import "@gouvfr/dsfr/dist/dsfr.min.css";
// import { zodResolver } from "@hookform/resolvers/zod";
// import { signIn } from "next-auth/react";
// import Link from "next/link";
// import { useRouter } from "next/navigation";
// import { useEffect, useState } from "react";
// import { useForm } from "react-hook-form";
// import { z } from "zod";

// import { ErrorCode } from "@calcom/features/auth/lib/ErrorCode";
// import { HOSTED_CAL_FEATURES, WEBAPP_URL, WEBSITE_URL } from "@calcom/lib/constants";
// import { getSafeRedirectUrl } from "@calcom/lib/getSafeRedirectUrl";
// import { useCompatSearchParams } from "@calcom/lib/hooks/useCompatSearchParams";
// import { useLocale } from "@calcom/lib/hooks/useLocale";
// import { collectPageParameters, telemetryEventTypes, useTelemetry } from "@calcom/lib/telemetry";
// import { trpc } from "@calcom/trpc/react";
// import { Alert, Button, EmailField, PasswordField } from "@calcom/ui";

// import type { inferSSRProps } from "@lib/types/inferSSRProps";
// import type { WithNonceProps } from "@lib/withNonce";

// import AddToHomescreen from "@components/AddToHomescreen";
// import PageWrapper from "@components/PageWrapper";
// import BackupCode from "@components/auth/BackupCode";
// import TwoFactor from "@components/auth/TwoFactor";

// import type { PageProps } from "~/auth/login-view";
// import LoginView from "~/auth/login-view";

// const Page = (props: PageProps) => <LoginView {...props} />;
// Page.PageWrapper = PageWrapper;

// const GoogleIcon = () => (
//   <img className="text-subtle mr-2 h-4 w-4 dark:invert" src="/google-icon.svg" alt="" />
// );
// export default function Login({
//   isSAMLLoginEnabled,
//   totpEmail,
// }: // eslint-disable-next-line @typescript-eslint/ban-types
// inferSSRProps<typeof getServerSideProps> & WithNonceProps<{}>) {
//   const searchParams = useCompatSearchParams();
//   const { t } = useLocale();
//   const router = useRouter();
//   const formSchema = z
//     .object({
//       email: z
//         .string()
//         .min(1, `${t("error_required_field")}`)
//         .email(`${t("enter_valid_email")}`),
//       ...(!!totpEmail ? {} : { password: z.string().min(1, `${t("error_required_field")}`) }),
//     })
//     // Passthrough other fields like totpCode
//     .passthrough();
//   const methods = useForm<LoginValues>({ resolver: zodResolver(formSchema) });
//   const { register, formState } = methods;
//   const [twoFactorRequired, setTwoFactorRequired] = useState(!!totpEmail || false);
//   const [twoFactorLostAccess, setTwoFactorLostAccess] = useState(false);
//   const [errorMessage, setErrorMessage] = useState<string | null>(null);

//   const errorMessages: { [key: string]: string } = {
//     // [ErrorCode.SecondFactorRequired]: t("2fa_enabled_instructions"),
//     // Don't leak information about whether an email is registered or not
//     [ErrorCode.IncorrectEmailPassword]: t("incorrect_email_password"),
//     [ErrorCode.IncorrectTwoFactorCode]: `${t("incorrect_2fa_code")} ${t("please_try_again")}`,
//     [ErrorCode.InternalServerError]: `${t("something_went_wrong")} ${t("please_try_again_and_contact_us")}`,
//     [ErrorCode.ThirdPartyIdentityProviderEnabled]: t("account_created_with_identity_provider"),
//   };

//   const telemetry = useTelemetry();

//   let callbackUrl = searchParams?.get("callbackUrl") || "";

//   if (/"\//.test(callbackUrl)) callbackUrl = callbackUrl.substring(1);

//   // If not absolute URL, make it absolute
//   if (!/^https?:\/\//.test(callbackUrl)) {
//     callbackUrl = `${WEBAPP_URL}/${callbackUrl}`;
//   }

//   const safeCallbackUrl = getSafeRedirectUrl(callbackUrl);

//   callbackUrl = safeCallbackUrl || "";

//   const LoginFooter = (
//     <Link href={`${WEBSITE_URL}/signup`} className="text-brand-500 font-medium">
//       {t("dont_have_an_account")}
//     </Link>
//   );

//   const TwoFactorFooter = (
//     <>
//       <Button
//         onClick={() => {
//           if (twoFactorLostAccess) {
//             setTwoFactorLostAccess(false);
//             methods.setValue("backupCode", "");
//           } else {
//             setTwoFactorRequired(false);
//             methods.setValue("totpCode", "");
//           }
//           setErrorMessage(null);
//         }}
//         StartIcon="arrow-left"
//         color="minimal">
//         {t("go_back")}
//       </Button>
//       {!twoFactorLostAccess ? (
//         <Button
//           onClick={() => {
//             setTwoFactorLostAccess(true);
//             setErrorMessage(null);
//             methods.setValue("totpCode", "");
//           }}
//           StartIcon="lock"
//           color="minimal">
//           {t("lost_access")}
//         </Button>
//       ) : null}
//     </>
//   );

//   const ExternalTotpFooter = (
//     <Button
//       onClick={() => {
//         window.location.replace("/");
//       }}
//       color="minimal">
//       {t("cancel")}
//     </Button>
//   );

//   const onSubmit = async (values: LoginValues) => {
//     setErrorMessage(null);
//     telemetry.event(telemetryEventTypes.login, collectPageParameters());
//     const res = await signIn<"credentials">("credentials", {
//       ...values,
//       callbackUrl,
//       redirect: false,
//     });
//     if (!res) setErrorMessage(errorMessages[ErrorCode.InternalServerError]);
//     // we're logged in! let's do a hard refresh to the desired url
//     else if (!res.error) router.push(callbackUrl);
//     else if (res.error === ErrorCode.SecondFactorRequired) setTwoFactorRequired(true);
//     else if (res.error === ErrorCode.IncorrectBackupCode) setErrorMessage(t("incorrect_backup_code"));
//     else if (res.error === ErrorCode.MissingBackupCodes) setErrorMessage(t("missing_backup_codes"));
//     // fallback if error not found
//     else setErrorMessage(errorMessages[res.error] || t("something_went_wrong"));
//   };

//   const { data, isPending, error } = trpc.viewer.public.ssoConnections.useQuery();

//   useEffect(
//     function refactorMeWithoutEffect() {
//       if (error) {
//         setErrorMessage(error.message);
//       }
//     },
//     [error]
//   );

//   useEffect(() => {
//     const script = document.createElement("script");
//     script.id = "lasuite-gaufre-script";
//     script.src = "https://integration.lasuite.numerique.gouv.fr/api/v1/gaufre.js";
//     script.async = true;
//     script.defer = true;
//     document.body.appendChild(script);

//     return () => {
//       document.body.removeChild(script);
//     };
//   }, []);

//   const displaySSOLogin = HOSTED_CAL_FEATURES
//     ? true
//     : isSAMLLoginEnabled && !isPending && data?.connectionExists;

//   const openSignIn = (event: any) => {
//     event.preventDefault();
//   };

//   return (
//     <div className="lasuite lasuite-homepage">
//       <header role="banner" className="fr-header lasuite-header">
//         <div className="fr-header__body">
//           <div className="fr-container lasuite-container">
//             <div className="fr-header__body-row">
//               <div className="fr-header__brand lasuite-header__brand fr-enlarge-link">
//                 <div className="fr-header__brand-top lasuite-header__brand-top">
//                   <div className="fr-header__logo">
//                     <p className="fr-logo">Gouvernement</p>
//                   </div>
//                 </div>
//                 <div className="fr-header__service lasuite-header__service">
//                   <a
//                     className="lasuite-header__service-link"
//                     href="/"
//                     title="Accueil - Dicalso"
//                     aria-label="Accueil - Dicalso">
//                     <img className="lasuite-header__service-logo fr-responsive-img" src="#" alt="" />
//                     <p className="fr-header__service-title lasuite-header__service-title">Dicalso</p>
//                   </a>
//                 </div>
//               </div>
//               <div className="fr-header__tools">
//                 <div
//                   className="fr-header__tools-links lasuite-header__tools-links"
//                   data-fr-js-header-links="true">
//                   <button
//                     type="button"
//                     className="lasuite-gaufre-btn  lasuite-gaufre-btn--vanilla js-lasuite-gaufre-btn"
//                     title="Les services de La Suite numérique">
//                     Les services de La Suite numérique
//                   </button>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </header>
//       <div className="lasuite-homepage__wrapper">
//         <div className="fr-container fr-p-0 lasuite-container">
//           <div className="lasuite-homepage__content">
//             <div className="fr-container--fluid">
//               <div className="fr-grid-row">
//                 <div className="lasuite-homepage__main-col">
//                   <div className="lasuite-homepage__tagline-container">
//                     <h1 className="lasuite-homepage__tagline">
//                       <strong className="lasuite-homepage__tagline-strong">Dicalso</strong>, un outil sécurisé
//                       <br />
//                       pour les agents de l&apos;État
//                     </h1>
//                   </div>
//                 </div>
//                 <div className="lasuite-homepage__secondary-col">
//                   <div className="lasuite-homepage__form-container">
//                     <div className="lasuite-homepage__form">
//                       <h2 className="fr-h4 fr-mb-8w lasuite-text-center">
//                         Connexion avec
//                         <br role="presentation" className="fr-hidden-sm" /> votre adresse e-mail
//                       </h2>
//                       <div className="lasuite-input-width">
//                         <form
//                           onSubmit={methods.handleSubmit(onSubmit)}
//                           noValidate
//                           data-testid="login-form"
//                           className="fr-mb-4w fr-mb-md-6w">
//                           <div className="fr-mb-4w fr-mb-md-6w">
//                             <EmailField
//                               className="fr-input lasuite-input fr-p-3w"
//                               id="email"
//                               defaultValue={totpEmail || (searchParams?.get("email") as string)}
//                               placeholder="Entrez votre adresse e-mail"
//                               required
//                               {...register("email")}
//                             />
//                             <PasswordField
//                               id="password"
//                               autoComplete="off"
//                               required={!totpEmail}
//                               className="fr-input lasuite-input fr-p-3w"
//                               {...register("password")}
//                             />
//                           </div>
//                           {twoFactorRequired ? (
//                             !twoFactorLostAccess ? (
//                               <TwoFactor center />
//                             ) : (
//                               <BackupCode center />
//                             )
//                           ) : null}

//                           {errorMessage && <Alert severity="error" title={errorMessage} />}

//                           <div className="text-center">
//                             <Button
//                               type="submit"
//                               disabled={formState.isSubmitting}
//                               className="fr-btn lasuite-btn w-full">
//                               {twoFactorRequired ? t("submit") : t("sign_in")}
//                             </Button>
//                           </div>
//                           <div className="mt-4 text-center">
//                             <Link
//                               href={`${WEBSITE_URL}/signup`}
//                               className="text-brand-500 d-block pb-2 pt-2 font-medium">
//                               {t("dont_have_an_account")}
//                             </Link>
//                           </div>
//                         </form>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//         <picture className="lasuite-homepage__bg">
//           <source
//             srcSet="https://integration.lasuite.numerique.gouv.fr/api/backgrounds/v1/default.avif"
//             type="image/avif"
//           />
//           <img
//             src="https://integration.lasuite.numerique.gouv.fr/api/backgrounds/v1/default.jpg"
//             alt=""
//             width="1920"
//             height="1200"
//           />
//         </picture>
//       </div>
//       <footer className="fr-footer" role="contentinfo" id="footer-7127">
//         <div className="fr-container lasuite-container">
//           <div className="fr-footer__body">
//             <div className="fr-footer__brand fr-enlarge-link">
//               <a id="footer-operator" href="/" title="Retour à l'accueil - Dicalso">
//                 <p className="fr-logo">Gouvernement</p>
//               </a>
//             </div>
//             <div className="fr-footer__content">
//               <p className="fr-footer__content-desc">
//                 Un service de la Direction interministérielle du numérique
//               </p>
//               <ul className="fr-footer__content-list">
//                 <li className="fr-footer__content-item">
//                   <a
//                     target="_blank"
//                     rel="noopener external"
//                     title="legifrance.gouv.fr - nouvelle fenêtre"
//                     className="fr-footer__content-link"
//                     href="https://legifrance.gouv.fr">
//                     legifrance.gouv.fr
//                   </a>
//                 </li>
//                 <li className="fr-footer__content-item">
//                   <a
//                     target="_blank"
//                     rel="noopener external"
//                     title="info.gouv.fr - nouvelle fenêtre"
//                     className="fr-footer__content-link"
//                     href="https://info.gouv.fr">
//                     info.gouv.fr
//                   </a>
//                 </li>
//                 <li className="fr-footer__content-item">
//                   <a
//                     target="_blank"
//                     rel="noopener external"
//                     title="service-public.fr - nouvelle fenêtre"
//                     className="fr-footer__content-link"
//                     href="https://service-public.fr">
//                     service-public.fr
//                   </a>
//                 </li>
//                 <li className="fr-footer__content-item">
//                   <a
//                     target="_blank"
//                     rel="noopener external"
//                     title="data.gouv.fr - nouvelle fenêtre"
//                     className="fr-footer__content-link"
//                     href="https://data.gouv.fr">
//                     data.gouv.fr
//                   </a>
//                 </li>
//               </ul>
//             </div>
//           </div>
//           <div className="fr-footer__bottom">
//             <ul className="fr-footer__bottom-list">
//               <li className="fr-footer__bottom-item">
//                 <a className="fr-footer__bottom-link" href="#">
//                   Plan du site
//                 </a>
//               </li>
//               <li className="fr-footer__bottom-item">
//                 <a className="fr-footer__bottom-link" href="#">
//                   Accessibilité&nbsp;: non conforme
//                 </a>
//               </li>
//               <li className="fr-footer__bottom-item">
//                 <a className="fr-footer__bottom-link" href="#">
//                   Mentions légales
//                 </a>
//               </li>
//               <li className="fr-footer__bottom-item">
//                 <a className="fr-footer__bottom-link" href="#">
//                   Données personnelles
//                 </a>
//               </li>
//               <li className="fr-footer__bottom-item">
//                 <a className="fr-footer__bottom-link" href="https://lasuite.numerique.gouv.fr/">
//                   La Suite Numérique
//                 </a>
//               </li>
//             </ul>
//             <div className="fr-footer__bottom-copy">
//               <p>
//                 Sauf mention explicite de propriété intellectuelle détenue par des tiers, les contenus de ce
//                 site sont proposés sous
//                 <a
//                   href="https://github.com/etalab/licence-ouverte/blob/master/LO.md"
//                   target="_blank"
//                   rel="noopener external"
//                   title="licence etalab-2.0 - nouvelle fenêtre">
//                   licence etalab-2.0
//                 </a>
//               </p>
//             </div>
//           </div>
//         </div>
//       </footer>
//       <AddToHomescreen />
//     </div>
//   );
// }

// export { getServerSideProps } from "@server/lib/auth/login/getServerSideProps";

// Login.PageWrapper = PageWrapper;
