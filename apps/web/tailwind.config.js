const base = require("@calcom/config/tailwind-preset");
/** @type {import('tailwindcss').Config} */
module.exports = {
  ...base,
  darkMode: ["selector", '[data-mode="dark"]'],
  content: [...base.content, "../../node_modules/@tremor/**/*.{js,ts,jsx,tsx}"],
  plugins: [...base.plugins, require("tailwindcss-animate")],
};
