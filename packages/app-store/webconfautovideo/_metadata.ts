import type { AppMeta } from "@calcom/types/App";

import _package from "./package.json";

export const metadata = {
  name: "WebConf Video",
  description: _package.description,
  installed: true,
  type: "webconfauto_video",
  variant: "conferencing",
  categories: ["conferencing"],
  logo: "icon.svg",
  publisher: "Cal.com",
  url: "https://jitsi.org/",
  slug: "webconfauto",
  title: "WebConf Video",
  isGlobal: false,
  email: "help@cal.com",
  appData: {
    location: {
      linkType: "dynamic",
      type: "integrations:webconfauto",
      label: "WebConfAuto Video",
    },
  },
  dirName: "webconfautovideo",
  concurrentMeetings: true,
  isOAuth: false,
} as AppMeta;

export default metadata;
