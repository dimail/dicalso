import { v4 as uuidv4 } from "uuid";

import type { EventBusyDate, CalendarEvent } from "@calcom/types/Calendar";
import type { PartialReference } from "@calcom/types/EventManager";
import type { VideoApiAdapter, VideoCallData } from "@calcom/types/VideoApiAdapter";

function shuffle(array: any[]) {
  let currentIndex = array.length,
    temporaryValue,
    randomIndex;

  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

function randomIntFromInterval(min: number, max: number) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateRoomName() {
  const charArray = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];
  const digitArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  const roomName =
    shuffle(digitArray).join("").substring(0, randomIntFromInterval(3, 6)) +
    shuffle(charArray).join("").substring(0, randomIntFromInterval(7, 10));
  return shuffle(roomName.split("")).join("");
}

const WebconfautoVideoApiAdapter = (): VideoApiAdapter => {
  return {
    createMeeting: async (event: CalendarEvent): Promise<VideoCallData> => {
      const hostUrl = "https://webconf.numerique.gouv.fr";

      const meetingID = uuidv4();

      return {
        type: "webconfauto_video",
        id: meetingID,
        password: "",
        url: `${hostUrl}/${generateRoomName()}`,
      };
    },
    updateMeeting: async (bookingRef: PartialReference, event: CalendarEvent): Promise<VideoCallData> => {
      // don't update WebConfauto link
      return {
        type: "webconfauto_video",
        id: bookingRef.meetingId as string,
        password: bookingRef.meetingPassword as string,
        url: bookingRef.meetingUrl as string,
      };
    },
    deleteMeeting: async (uid: string): Promise<unknown> => {
      // delete WebConfauto link
      return {};
    },
    getAvailability: async (dateFrom?: string, dateTo?: string): Promise<EventBusyDate[]> => {
      // get WebConfauto availability
      return [];
    },
  };
};

export default WebconfautoVideoApiAdapter;
