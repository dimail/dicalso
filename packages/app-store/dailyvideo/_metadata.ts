import type { AppMeta } from "@calcom/types/App";

export const metadata = {
  name: "Dailyvideo",
  description: "dailyvideo",
  installed: true,
  type: "daily_video",
  variant: "conferencing",
  url: "",
  categories: ["conferencing"],
  logo: "icon.svg",
  publisher: "",
  category: "conferencing",
  slug: "daily-video",
  title: "dailyvideo",
  isGlobal: true,
  email: "",
  appData: {
    location: {
      linkType: "dynamic",
      type: "integrations:daily",
      label: "dailyvideo",
    },
  },
  key: { apikey: process.env.DAILY_API_KEY },
  dirName: "dailyvideo",
  isOAuth: false,
} as AppMeta;

export default metadata;
