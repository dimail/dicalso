import { validJson } from "@calcom/lib/jsonUtils";
import type { AppMeta } from "@calcom/types/App";

import _package from "./package.json";

export const metadata = {
  name: "WebConf",
  description: _package.description,
  installed: !!(process.env.GOOGLE_API_CREDENTIALS && validJson(process.env.GOOGLE_API_CREDENTIALS)),
  type: "webconf",
  title: "WebConf",
  variant: "conferencing",
  category: "conferencing",
  categories: ["conferencing"],
  logo: "icon.svg",
  publisher: "Dinum",
  slug: "webconf",
  url: "https://example.com/link",
  email: "help@cal.com",
  dirName: "googlecalendar",
  isOAuth: true,
} as AppMeta;

export default metadata;
