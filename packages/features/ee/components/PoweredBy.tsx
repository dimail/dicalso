import { useSession } from "next-auth/react";
import Link from "next/link";

import { useIsEmbed } from "@calcom/embed-core/embed-iframe";
import { APP_NAME } from "@calcom/lib/constants";
import { useLocale } from "@calcom/lib/hooks/useLocale";

const PoweredByCal = ({ logoOnly }: { logoOnly?: boolean }) => {
  const { t } = useLocale();
  const session = useSession();
  const isEmbed = useIsEmbed();
  const hasValidLicense = session.data ? session.data.hasValidLicense : null;

  return (
    <div className="p-2 text-center text-xs sm:text-right">
      <Link href="https://lasuite.numerique.gouv.fr/" target="_blank" className="text-subtle">
        <>{t("powered_by")} </>
        {APP_NAME === "Dicalso" || !hasValidLicense ? (
          <>
            <img className="relative inline" src="/api/logo" alt="Dicalso" />
          </>
        ) : (
          <span className="text-emphasis font-semibold opacity-50 hover:opacity-100">{APP_NAME}</span>
        )}
      </Link>
    </div>
  );
};

export default PoweredByCal;
