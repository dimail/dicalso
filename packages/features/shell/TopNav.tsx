import { useSession } from "next-auth/react";
import Link from "next/link";
import React, { useEffect, useState } from "react";

import { useIsEmbed } from "@calcom/embed-core/embed-iframe";
import { KBarTrigger } from "@calcom/features/kbar/Kbar";
import { useLocale } from "@calcom/lib/hooks/useLocale";
import { Icon, Logo } from "@calcom/ui";

import { UserDropdown } from "./user-dropdown/UserDropdown";

export function TopNavContainer() {
  const { status } = useSession();
  if (status !== "authenticated") return null;
  return <TopNav />;
}

function TopNav() {
  const isEmbed = useIsEmbed();
  const { t } = useLocale();
  const [openGaufre, setOpenGaufre] = useState(false);

  useEffect(() => {
    const script = document.createElement("script");
    script.id = "lasuite-gaufre-script";
    script.src = "https://integration.lasuite.numerique.gouv.fr/api/v1/gaufre.js";
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  useEffect(() => {
    const popup = document.getElementById("lasuite-gaufre-popup");
    if (popup) {
      popup.style.setProperty("position", "fixed", "important");
    }
  }, []);

  return (
    <>
      <nav
        style={isEmbed ? { display: "none" } : {}}
        className="bg-muted border-subtle sticky top-0 z-40 flex w-full items-center justify-between border-b bg-opacity-50 px-4 py-1.5 backdrop-blur-lg sm:p-4 md:hidden">
        <Link href="/event-types">
          <Logo />
        </Link>
        <div className="flex items-center gap-2 self-center">
          <span className="hover:bg-muted hover:text-emphasis text-default group flex items-center rounded-full text-sm font-medium transition lg:hidden">
            <KBarTrigger />
          </span>
          <button className="hover:bg-muted hover:text-subtle text-muted rounded-full p-1 transition focus:outline-none focus:ring-2 focus:ring-black focus:ring-offset-2">
            <span className="sr-only">{t("settings")}</span>
            <Link href="/settings/my-account/profile">
              <Icon name="settings" className="text-default h-4 w-4" aria-hidden="true" />
            </Link>
          </button>
          <UserDropdown small />
          <div className="lagaufre-button" onClick={() => setOpenGaufre(!openGaufre)}>
            <button
              type="button"
              className="lasuite-gaufre-btn--vanilla lasuite--gaufre-loaded js-lasuite-gaufre-btn"
              title="Les services de La Suite numérique">
              <span className="js-lasuite-gaufre-btn hidden w-full justify-between lg:flex">
                La Suite numérique
              </span>
            </button>
          </div>
        </div>
      </nav>
    </>
  );
}
